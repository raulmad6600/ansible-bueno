#!/bin/bash
PROJECT_ID="49097184"
GITLAB_BOT_USER="bhenriedev"
GITLAB_BOT_TOKEN=${GITLAB_TOKEN}
STATE_NAME="proxmox"

terraform init -migrate-state \
      -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}" \
      -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock" \
      -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock" \
      -backend-config="username=$GITLAB_BOT_USER" \
      -backend-config="password=$GITLAB_BOT_TOKEN" \
      -backend-config="lock_method=POST" \
      -backend-config="unlock_method=DELETE" \
      -backend-config="retry_wait_min=5"