resource "proxmox_vm_qemu" "k3s_master_node_bootstrap" {
  vmid        = 300
  name        = "master-node-boot"
  tags        = "k3s,master"
  target_node = "proxmox"
  desc        = "master bootstrap k3s node"
  os_type     = "cloud-init"
  ciuser      = var.user
  cipassword  = var.password
  sshkeys     = file("${path.module}/../inventory/public_keys")
  clone       = "ubuntu-cloud"  # Ensure this matches your template name or ID
  full_clone  = true
  onboot      = true
  agent       = local.agent
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  balloon     = 1024
  disk {
    backup             = 0
    cache              = "none"
    format             = "raw"
    size               = "80G"
    storage            = "local-lvm"
    type               = "scsi"
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
    tag    = "4"
  }
  ipconfig0  = "ip=10.10.4.60/24,gw=10.10.4.1"
  nameserver = "10.10.4.1"

}

resource "proxmox_vm_qemu" "k3s_master_node_1" {
  vmid        = 301
  name        = "master-node-1"
  tags        = "k3s,master"
  target_node = "proxmox"
  desc        = "master k3s node"
  os_type     = "cloud-init"
  ciuser      = var.user
  cipassword  = var.password
  sshkeys     = file("../inventory/public_keys")
  clone       = "ubuntu-cloud"  # This is the VM/template ID you want to clone
  onboot      = true
  agent       = local.agent
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  balloon     = 1024
   disk {
    backup             = 0
    cache              = "none"
    format             = "raw"
    size               = "80G"
    storage            = "local-lvm"
    type               = "scsi"
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
    tag    = "4"
  }
  ipconfig0  = "ip=10.10.4.61/24,gw=10.10.4.1"
  nameserver = "10.10.4.1"
}

resource "proxmox_vm_qemu" "k3s_master_node_2" {
  vmid        = 302
  name        = "master-node-2"
  tags        = "k3s,master"
  target_node = "proxmox"
  desc        = "master k3s node"
  os_type     = "cloud-init"
  ciuser      = var.user
  cipassword  = var.password
  sshkeys     = file("../inventory/public_keys")
  clone       = "ubuntu-cloud"  # This is the VM/template ID you want to clone
  onboot      = true
  agent       = local.agent
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 4096
  balloon     = 1024
    disk {
    backup             = 0
    cache              = "none"
    format             = "raw"
    size               = "80G"
    storage            = "local-lvm"
    type               = "scsi"
  }
  network {
    bridge = "vmbr0"
    model  = "virtio"
    tag    = "4"
  }
  ipconfig0  = "ip=10.10.4.62/24,gw=10.10.4.1"
  nameserver = "10.10.4.1"
}

