# kubevip Ansible Role

This Ansible role is crafted for setting up and configuring `kube-vip` in a K3s cluster. The `kube-vip` component provides a virtual IP to enable highly available Kubernetes control planes.

## Requirements

- Ansible version 2.1 or newer.
- SSH access to the bootstrap node.
- k3s should already be installed on the bootstrap node.
- `systemctl` must be available on the target node to check k3s's status.

## Role Variables

Variables are sourced from the inventory's `vault.yml` and `group_vars/main-cluster.yml`.

**Main Variables**:
- `kube_vip_ip`: The virtual IP for kube-vip.
- `kube_vip_sans_names`: Additional Subject Alternative Names for kube-vip.
- `kube_vip_interface`: Network interface to bind the virtual IP.

**Sensitive Data (from `vault.yml`)**:
- `ansible_ssh_private_key`: The private SSH key for the Ansible user.
- `k3s_node_token`: Token used for k3s nodes.
- `k3s_kubeconfig`: The kubeconfig for the k3s cluster.

## Dependencies

None.

## Example Playbook

```yaml
- name: Setup k3s kube-vip
  hosts: bootstrap
  become: yes
  gather_facts: no
  vars_files:
    - ../inventory/group_vars/main-cluster.yml
    - ../inventory/vault.yml
  roles:
    - roles/kubevip
```

**Pre-tasks**:

Before executing the main tasks of the role:

- Create a temporary SSH key file on the Ansible controller.
- Set the `ansible_ssh_private_key_file` variable to point to this temporary key.

**Post-tasks**:

After the role's main tasks:

- Print the k3s node token.
- Print the k3s kubeconfig.
- Remove the temporary SSH key file from the Ansible controller.

## License

GPL-2.0-or-later (or specify your preferred license).

## Author Information

This role was devised in 2023 by Brock Henrie, Spakl.
