#!/bin/bash

ssh-keygen -t ed25519 -f ./ansible -C "ansible"

echo "" 
echo "Private Key"
cat ./ansible
echo ""
echo ""
echo "Public Key"
cat ./ansible.pub