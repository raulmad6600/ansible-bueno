encrypt:
	ansible-vault encrypt inventory/vault.yml

decrypt:
	ansible-vault decrypt inventory/vault.yml

maintenance:
	ansible-playbook -i inventory/hosts.yml playbooks/server_maintenance.yml

main-cluster:
	ansible-playbook -i inventory/hosts.yml playbooks/main_cluster.yml

uninstall_k3s:
	ansible-playbook -i inventory/hosts.yml playbooks/uninstall_k3s_masters.yml